---
＃特意从我的个人blog教程上精简过来了
安装环境：efi+gpt分区。一块硬盘就可以了（这个硬盘需要全盘格式化）
---
Arch面向高级用户，缺乏技术专长或者没有耐心的人是无法顺利安装、如何安装Arch。ArchWiki上列举了[ArchLinux的安装步骤](https://wiki.archlinux.org/index.php/Installation_guide_(%E7%AE%80%E4%BD%93%E4%B8%AD%E6%96%87)　)，需要用户在黑色的tty界面下输入如下多个命令：

```
sudo systemctl start dhcpcd #我用的是Android手机做WiFi无线网卡
ping baidu.com #ctrl+c终止进程
ls /sys/firmware/efi/efivars
timedatectl
timedatectl set-ntp true
timedatectl
fdisk -l
lsblk
cfdisk /dev/sda
cfdisk /dev/sdb
lsblk
mkfs.fat -F32 /dev/sda1
mkfs.ext4 /dev/sda2
mkfs.ext4 /dev/sdb1
mount /dev/sda2 /mnt
mkdir /mnt/boot
mkdir /mnt/home
mount /dev/sda1 /mnt/boot
mount /dev/sdb1 /mnt/home
lsblk
nano /etc/pacman.d/mirrorlist
pacstrap -i /mnt base base-devel
genfstab -U /mnt >> /mnt/etc/fstab
cat /mnt/etc/ifstab #三个分区必须添加进来，否则请查原因
arch-chroot /mnt
ln -sf /usr/share/zoneinfo/Asia/Shanghai /etc/localtime
hwclock --systohc --utc
nano /etc/locale.gen
en_US.UTF-8 UTF-8
zh_CN.UTF-8 UTF-8
zh_TW.UTF-8 UTF-8
locale-gen
echo LANG=en_US.UTF-8 > /etc/locale.conf
echo archlinux > /etc/hostname
nano /etc/hosts
127.0.0.1	localhost
::1		    localhost
127.0.1.1	archlinux.localdomain	archlinux
pacman -S iw wpa_supplicant dialog
mkinitcpio -p linux
passwd
pacman -S grub efibootmgr os-prober
grub-install --target=x86_64-efi --efi-directory=/boot --bootloader-id=arch_grub
grub-mkconfig -o /boot/grub/grub.cfg
pacman -S  intel-ucode
grub-mkconig -o /boot/grub/grub.cfg
useradd -m [你的用户名]
passwd [你的用户名]
nano /etc/sudoers
[你的用户名] ALL=(ALL) ALL
[你的用户名] ALL=(ALL) NOPASSWD: ALL
exit
exit
exit
umount -R /mnt
reboot

# 进入自己的账户
sudo systemctl start dhcpcd
sudo pacman -S xfce4 xfce4-goodies lightdm lightdm-gtk-greeter lightdm-gtk-greeter-settings networkmanager network-manager-applet dhclient gnome-keyring seahorse libsecret gnome-keyring ntfs-3g gvfs gvfs-mtp dosfstools alsa-utils pavucontrol pulseaudio xorg xorg-xinit xterm xorg-xclock xorg-twm mesa xf86-video-intel ttf-dejavu wqy-zenhei 
startx #需要用下面命令把最左边的窗口kill
exit
sudo systemctl enable lightdm
sudo systemctl enable NetworkManager
sudo reboot
# 这里只是安装了一部分包，但是好消息的是xfce4桌面已经装上去了。功能正常。
# 别的还有aur，预编译源的包需要自己添加，还有各种美化+优化
```

